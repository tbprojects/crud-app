module.exports = [
  {
    'id': 1,
    'firstName': 'John',
    'lastName': 'Rambo',
    'bio': 'Lorem ipsum',
    'gender': 'male',
    'birthDate': { "year": 1990, "month": 4, "day": 4 }
  },
  {
    'id': 2,
    'firstName': 'Chuk',
    'lastName': 'Norris',
    'bio': 'Lorem ipsum',
    'gender': 'male',
    'birthDate': { "year": 1980, "month": 1, "day": 31 }
  },
  {
    'id': 3,
    'firstName': 'Jodie',
    'lastName': 'Foster',
    'bio': 'Lorem ipsum',
    'gender': 'female',
    'birthDate': { "year": 1994, "month": 10, "day": 22 }
  },
  {
    'id': 4,
    'firstName': 'Sandra',
    'lastName': 'Bullock',
    'bio': 'Lorem ipsum',
    'gender': 'female',
    'birthDate': { "year": 1985, "month": 4, "day": 1 }
  }
];
