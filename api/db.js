module.exports = () => {
  return {
    contacts: require('./resources/contacts'),
    users: require('./resources/users'),
    teams: require('./resources/teams')
  }
};
