import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const countryNumbers = ['+1', '+48', '+55', '+22'];

@Component({
  selector: 'app-phone-selector',
  template: `
    <select [disabled]="disabled" [(ngModel)]="countryNumber" (change)="updateNumber()" (blur)="onTouched()">
      <option *ngFor="let number of countryNumbers" [value]="number">{{number}}</option>
    </select>
    <input [disabled]="disabled" type="text" [(ngModel)]="phoneNumber"
           (keyup)="updateNumber()" (blur)="onTouched()" placeholder="Phone number">
  `,
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => PhoneSelectorComponent), multi: true}
  ],
  styleUrls: ['./phone-selector.component.css']
})
export class PhoneSelectorComponent implements ControlValueAccessor {
  countryNumbers = countryNumbers;

  countryNumber: string;
  phoneNumber: string;
  disabled;
  private onChange = (value: string) => {};
  onTouched = () => {};

  updateNumber() {
    console.log('new value', `${this.countryNumber} ${this.phoneNumber}`);
    this.onChange(`${this.countryNumber} ${this.phoneNumber}`);
  }

  writeValue(phoneNumber: string): void {
    const values = phoneNumber.split(' ');
    this.countryNumber = values.shift();
    this.phoneNumber = values.join();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
