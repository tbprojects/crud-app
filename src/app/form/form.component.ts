import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, pluck, switchMap, tap } from 'rxjs/operators';
import { ResourceService } from '../resource.service';
import { resourceConfigs } from '../resources';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {
  resourceConfig = this.route.parent.params.pipe(
    map((params) => resourceConfigs.find((config) => config.key === params.resourceType)),
    tap((resourceConfig) => this.resourceType = resourceConfig.key)
  );

  resource = this.resourceConfig.pipe(
    switchMap((resourceConfig) => this.route.params.pipe(
      switchMap((params) => this.resourceService.getResource(resourceConfig.key, params.id))
    ))
  );

  resourceType: string;
  form = new FormGroup({});

  constructor(private route: ActivatedRoute,
              private router: Router,
              private resourceService: ResourceService) { }

  save(resource) {
    this.resourceService.saveResource(this.resourceType, resource)
      .subscribe(this.onSaveSuccess.bind(this), this.onSaveFailure.bind(this));
  }

  private onSaveSuccess(resource) {
    this.router.navigate(['..'], {relativeTo: this.route});
  }

  private onSaveFailure() {
    alert('Could not be saved!');
  }
}
