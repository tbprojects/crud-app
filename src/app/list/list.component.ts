import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResourceService } from '../resource.service';
import { map, pluck, switchMap } from 'rxjs/operators';
import { resourceConfigs } from '../resources';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {
  resourceConfig = this.route.params.pipe(
    map((params) => resourceConfigs.find((config) => config.key === params.resourceType))
  );
  resources = this.resourceConfig.pipe(
    switchMap((resourceConfig) => this.resourceService.getResources(resourceConfig.key))
  );
  listColumns = this.resourceConfig.pipe(pluck('listColumns'));
  headline = this.resourceConfig.pipe(pluck('name'));

  constructor(private route: ActivatedRoute,
              private router: Router,
              private resourceService: ResourceService) { }

  edit(event) {
    if (event.type === 'click') {
      this.router.navigate([event.row.id], {relativeTo: this.route});
    }
  }

}
