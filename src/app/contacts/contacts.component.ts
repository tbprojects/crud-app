import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { Contact } from '../reducers/contacts.models';
import * as fromRoot from '../reducers/index';
import * as fromContacts from '../reducers/contacts.reducer';
import * as ContactsActions from '../reducers/contacts.actions';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent {
  contactsState: Observable<fromContacts.State> = this.store.pipe(
    select(fromRoot.selectContacts)
  );

  contacts: Observable<Contact[]> = this.contactsState.pipe(
    map((state) => fromContacts.selectAll(state))
  );

  loading: Observable<boolean> = this.contactsState.pipe(
    map((state) => state.loading)
  );

  loadingFailed: Observable<boolean> = this.contactsState.pipe(
    map((state) => state.loadingFailed)
  );

  constructor(private store: Store<fromRoot.State>) {}

  load() {
    this.store.dispatch(new ContactsActions.Load());
  }

  add(contact: Contact) {
    this.store.dispatch(new ContactsActions.Add(contact));
  }

  remove(contact: Contact) {
    this.store.dispatch(new ContactsActions.Remove(contact));
  }

  clear() {
    this.store.dispatch(new ContactsActions.Clear());
  }

}
