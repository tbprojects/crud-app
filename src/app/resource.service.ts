import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Resource } from './resources';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {

  constructor(private http: HttpClient) { }

  getResources(resourceType: string) {
    return this.http.get<object[]>(`/api/${resourceType}`);
  }

  getResource(resourceType: string, id: number) {
    return this.http.get<object>(`/api/${resourceType}/${id}`);
  }

  saveResource(resourceType, resource: Resource) {
    if (resource.id) {
      return this.http.put<object>(`/api/${resourceType}/${resource.id}`, resource);
    } else {
      return this.http.post<object>(`/api/${resourceType}`, resource);
    }
  }

  deleteResource(resourceType, id: number) {
    return this.http.delete<object>(`/api/${resourceType}/${id}`);
  }
}
