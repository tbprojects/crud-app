import { FormlyFieldConfig } from '@ngx-formly/core/lib/components/formly.field.config';
import { TableColumn } from '@swimlane/ngx-datatable/src/types/table-column.type';

export interface ResourceConfig {
  key: string;
  name: string;
  listColumns: TableColumn[];
  formFields: FormlyFieldConfig[];
  formHeader?: Function;
}

export interface Resource {
  id: any;
}

/*
  Here you can register resource configs
 */

export const resourceConfigs: ResourceConfig[] = [
  require('./resources/teams.ts').resourceConfig,
  require('./resources/users.ts').resourceConfig
];
