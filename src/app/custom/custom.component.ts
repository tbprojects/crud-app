import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-custom',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.css']
})
export class CustomComponent implements OnInit {

  control = new FormControl('+48 123456', {validators: [Validators.required]});

  constructor() { }

  ngOnInit() {
  }

}
