import { Action } from '@ngrx/store';
import { Contact } from './contacts.models';

export enum ContactActions {
  ADD = '[Contacts] Add',
  REMOVE = '[Contacts] Remove',
  CLEAR = '[Contacts] Clear',
  LOAD = '[Contacts] Load',
  LOADED = '[Contacts] Loaded',
  LOAD_FAILED = '[Contacts] Load failed',
}

export class Add implements Action {
  readonly type = ContactActions.ADD;
  constructor(public payload: Contact) {}
}

export class Remove implements Action {
  readonly type = ContactActions.REMOVE;
  constructor(public payload: Contact) {}
}

export class Clear implements Action {
  readonly type = ContactActions.CLEAR;
}

export class Load implements Action {
  readonly type = ContactActions.LOAD;
}

export class Loaded implements Action {
  readonly type = ContactActions.LOADED;
  constructor(public payload: Contact[]) {}
}

export class LoadFailed implements Action {
  readonly type = ContactActions.LOAD_FAILED;
}

export type ContactsActionsUnion = Add | Remove | Clear | Load | Loaded | LoadFailed;
