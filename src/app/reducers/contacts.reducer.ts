import { ContactActions, ContactsActionsUnion } from './contacts.actions';
import { Contact } from './contacts.models';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export const adapter: EntityAdapter<Contact> = createEntityAdapter<Contact>();
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();

export interface State extends EntityState<Contact> {
  loading: boolean;
  loadingFailed: boolean;
}

export const initialState: State = adapter.getInitialState({
  loading: false,
  loadingFailed: false
});

export function reducer(state = initialState, action: ContactsActionsUnion): State {
  let newState: State;
  switch (action.type) {

    case ContactActions.ADD:
      return adapter.upsertOne(action.payload, state);

    case ContactActions.REMOVE:
      return adapter.removeOne(action.payload.id, state);

    case ContactActions.CLEAR:
      return adapter.removeAll(state);

    case ContactActions.LOAD:
      return {...state, loading: true};

    case ContactActions.LOADED:
      newState = adapter.removeAll(state);
      newState = adapter.addMany(action.payload, state);
      return {...newState, loading: false, loadingFailed: false};

    case ContactActions.LOAD_FAILED:
      newState = adapter.removeAll(state);
      return {...newState, loading: false, loadingFailed: true};

    default:
      return state;
  }
}
