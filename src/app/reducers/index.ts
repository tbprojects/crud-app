import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';

import * as fromContacts from './contacts.reducer';
import { ContactsEffects } from './contacts.effects';

export interface State {
  contacts: fromContacts.State;
}

export const reducers: ActionReducerMap<State> = {
  contacts: fromContacts.reducer
};

export const effects = [ContactsEffects];

export const selectContacts = createFeatureSelector<fromContacts.State>('contacts');
