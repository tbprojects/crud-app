import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { ContactActions } from './contacts.actions';
import { HttpClient } from '@angular/common/http';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as ContactsActions from '../reducers/contacts.actions';
import { Contact } from './contacts.models';
import { Action } from '@ngrx/store';

@Injectable({providedIn: 'root'})
export class ContactsEffects {

  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType(ContactActions.LOAD),
    switchMap((action) =>
      this.http.get<Contact[]>('/api/contacts').pipe(
        map((contacts) => new ContactsActions.Loaded(contacts)),
        catchError(() => of(new ContactsActions.LoadFailed()))
      )
    )
  );

  constructor(private http: HttpClient, private actions$: Actions) {}
}
