import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ResourceNavComponent } from './resource-nav/resource-nav.component';
import { EmptyComponent } from './empty/empty.component';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { DatepickerComponent } from './inputs/datepicker/datepicker.component';
import { CustomComponent } from './custom/custom.component';
import { PhoneSelectorComponent } from './phone-selector/phone-selector.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { reducers, effects } from './reducers';
import { environment } from '../environments/environment';
import { ContactsComponent } from './contacts/contacts.component';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [
    AppComponent,
    ResourceNavComponent,
    EmptyComponent,
    ListComponent,
    FormComponent,
    DatepickerComponent,
    CustomComponent,
    PhoneSelectorComponent,
    ContactsComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    FormlyModule.forRoot({
      types: [{
        name: 'datepicker', component: DatepickerComponent, extends: 'input'
      }]
    }),
    FormlyBootstrapModule,
    NgbModule.forRoot(),
    NgxDatatableModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects),
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
