import { Component } from '@angular/core';
import { Field } from '@ngx-formly/core';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css']
})
export class DatepickerComponent extends Field {
  static format(date: {year: number, month: number, day: number}): string {
    return `${date.year}-${date.month}-${date.day}`;
  }
}
