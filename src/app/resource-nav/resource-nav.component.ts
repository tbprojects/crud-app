import { Component } from '@angular/core';
import { resourceConfigs } from '../resources';

@Component({
  selector: 'app-resource-nav',
  templateUrl: './resource-nav.component.html',
  styleUrls: ['./resource-nav.component.css']
})
export class ResourceNavComponent {
  resourceConfigs = resourceConfigs;
}
