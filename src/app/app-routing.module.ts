import { RouterModule, Routes } from '@angular/router';
import { EmptyComponent } from './empty/empty.component';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { NgModule } from '@angular/core';
import { CustomComponent } from './custom/custom.component';
import { ContactsComponent } from './contacts/contacts.component';

const routes: Routes = [
  {
    path: '',
    component: EmptyComponent,
    pathMatch: 'full'
  },
  {
    path: 'contacts',
    component: ContactsComponent
  },
  {
    path: 'custom',
    component: CustomComponent
  },
  {
    path: ':resourceType',
    component: ListComponent,
    children: [
      {
        path: '',
        component: EmptyComponent,
        pathMatch: 'full'
      },
      {
        path: ':id',
        component: FormComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
