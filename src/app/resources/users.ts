import { ResourceConfig } from '../resources';
import { DatePipe } from '@angular/common';
import { DatepickerComponent } from '../inputs/datepicker/datepicker.component';

export const resourceConfig: ResourceConfig = {
  key: 'users',
  name: 'Users',
  listColumns: [
    { prop: 'firstName', name: 'Imię' },
    { prop: 'lastName', name: 'Nazwisko' },
    { prop: 'bio', name: 'Biografia' },
    { prop: 'gender', name: 'Płeć' },
    { prop: 'birthDate', name: 'Data urodzenia', pipe: {transform: DatepickerComponent.format} }
  ],
  formFields: [
    { key: 'id', type: 'input', hideExpression: true },
    { key: 'firstName', type: 'input', templateOptions: {label: 'Imię'} },
    { key: 'lastName', type: 'input', templateOptions: {label: 'Nazwisko'} },
    { key: 'bio', type: 'textarea', templateOptions: {label: 'Biografia'} },
    {
      key: 'gender',
      type: 'select',
      templateOptions: { label: 'Płeć', options: [{ label: 'Male', value: 'male' }, { label: 'Female', value: 'female' }]}
    },
    { key: 'birthDate', templateOptions: {label: 'Data urodzenia'}, type: 'datepicker' },
  ]
};
