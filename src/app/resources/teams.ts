import { ResourceConfig } from '../resources';

export const resourceConfig: ResourceConfig = {
  key: 'teams',
  name: 'Teams',
  listColumns: [
    { prop: 'name', name: 'Nazwa drużyny' },
    { prop: 'description', name: 'Opis' }
  ],
  formFields: [
    { key: 'id', type: 'input', hideExpression: true },
    { key: 'name', type: 'input', templateOptions: {label: 'Nazwa drużyny'} },
    { key: 'description', type: 'textarea', templateOptions: {label: 'Opis'} },
  ],
  formHeader: (resource) => resource.name
};
